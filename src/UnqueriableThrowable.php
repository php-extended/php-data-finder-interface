<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-finder-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataFinder;

use Throwable;

/**
 * UnqueriableThrowable interface file.
 *
 * Unqieriable Exceptions are thrown then the data is unreachable for any
 * reason, either its source is unavailable, or it can't be read, or it can't
 * be decoded, decrypted, unmarshalled, etc.
 *
 * @author Anastaszor
 */
interface UnqueriableThrowable extends Throwable
{
	
	// nothing to add
	
}
