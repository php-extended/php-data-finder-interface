<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-data-finder-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\DataFinder;

use Iterator;
use Stringable;

/**
 * DataFinderInterface interface file.
 * 
 * This interface represents a finder that has access to some data and is able
 * to transform this data into objects, and to query from this data source with
 * simple query orders based on value matching, multi-layer ordering, and with
 * offset-limit window.
 * 
 * @author Anastaszor
 * @template T of object
 */
interface DataFinderInterface extends Stringable
{
	
	public const ORDER_ASC = false;
	public const ORDER_DESC = true;
	
	/**
	 * Gets the object with the given identifier, or null if it does not exist.
	 * 
	 * @param string $identifier
	 * @return ?T
	 * @throws UnqueriableThrowable
	 */
	public function findById(string $identifier) : ?object;
	
	/**
	 * Gets the first object that matches the attribute values within the offset
	 * and limit window, ordered by the orderings. The attributes is an array
	 * with keys as attribute names and values as queried attribute values.
	 * If attribute keys are not present within the data objects, then an
	 * empty array will be returned as no objects has attributes with this name.
	 * This search returns objects that matches exactly the values queried into
	 * the attribute values. If no attribute values are provided, all objects
	 * matches this query.
	 * The ordering is an array with keys as attribute names and values as the
	 * ordering values (use this class' constants). If an ordering key is not
	 * present within the attributes of the objects, it is just ignored.
	 * 
	 * @param array<string, string> $attributeValues
	 * @param array<string, bool> $orderings
	 * @param integer $offset
	 * @param integer $limit
	 * @return ?T
	 * @throws UnqueriableThrowable
	 */
	public function findFirstMatch(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : ?object;
	
	/**
	 * Gets all the objects that matches the attribute values within the offset
	 * and limit window, ordered by the orderings. The attributes is an array
	 * with keys as attribute names and values as queried attribute values.
	 * If attribute keys are not present within the data objects, then an
	 * empty array will be returned as no objects has attributes with this name.
	 * This search returns objects that matches exactly the values queried into
	 * the attribute values. If no attribute values are provided, all objects
	 * matches this query.
	 * The ordering is an array with keys as attribute names and values as the
	 * ordering values (use this class' constants). If an ordering key is not
	 * present within the attributes of the objects, it is just ignored.
	 * 
	 * @param array<string, string> $attributeValues
	 * @param array<string, bool> $orderings
	 * @param integer $offset
	 * @param integer $limit
	 * @return Iterator<T>
	 * @throws UnqueriableThrowable
	 */
	public function findMatches(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : Iterator;
	
	/**
	 * Gets the first object that matches the attribute values within the offset
	 * and limit window, ordered by the orderings. The attributes is an array
	 * with keys as attribute names and values as queried attribute values.
	 * If attribute keys are not present within the data objects, then an
	 * empty array will be returned as no objects has attributes with this name.
	 * This search returns objects that matches the values queried into the
	 * attribute values, with the same syntax as a LIKE statement in SQL.
	 * If no attribute values are provided, all objects matches this query.
	 * The ordering is an array with keys as attribute names and values as the
	 * ordering values (use this class' constants). If an ordering key is not
	 * present within the attributes of the objects, it is just ignored.
	 * 
	 * @param array<string, string> $attributeValues
	 * @param array<string, bool> $orderings
	 * @param integer $offset
	 * @param integer $limit
	 * @return ?T
	 * @throws UnqueriableThrowable
	 */
	public function findFirstLike(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : ?object;
	
	/**
	 * Gets all the objects that matches the attribute values within the offset
	 * and limit window, ordered by the orderings. The attributes is an array
	 * with keys as attribute names and values as queried attribute values.
	 * If attribute keys are not present within the data objects, then an
	 * empty array will be returned as no objects has attributes with this name.
	 * This search returns objects that matches the values queried into the
	 * attribute values, with the same syntax as a LIKE statement in SQL.
	 * If no attribute values are provided, all objects matches this query.
	 * The ordering is an array with keys as attribute names and values as the
	 * ordering values (use this class' constants). If an ordering key is not
	 * present within the attributes of the objects, it is just ignored.
	 * 
	 * @param array<string, string> $attributeValues
	 * @param array<string, bool> $orderings
	 * @param integer $offset
	 * @param integer $limit
	 * @return Iterator<T>
	 * @throws UnqueriableThrowable
	 */
	public function findLike(array $attributeValues, array $orderings = [], int $offset = 0, int $limit = 10) : Iterator;
	
}
